# frozen_string_literal: true

require 'json'

Facter.add(:mmp_config) do
  # https://puppet.com/docs/puppet/latest/fact_overview.html
  confine :kernelrelease do |value|
    value.include?('mmp')
  end
  setcode do
    JSON.parse(File.read('/mmp-config/config.json')) if File.exist?('/mmp-config/config.json')
  end
end
