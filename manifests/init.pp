# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include mmpos
class mmpos(Hash $config = {}) {
  file{"/home/miner/${facts[networking][mac]}.yaml":
    ensure  => present,
    content => {'mmpos::config' => $facts[mmp_config]}.to_yaml
  }
  if $config.keys.sort.join == 'idrig_code' {
    file{'/mmp-config/config.json':
      ensure  => file,
      replace => false,
      content => $config.to_json
    }
  }
}
